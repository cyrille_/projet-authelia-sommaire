# projet authelia 3 : Déploiement infra en 3 étapes

  -  [Pipeline 1 : Flask](https://gitlab.com/cyrille_/projet-flask-authelia)
  -  [Pipeline 2 : Packer Ansible](https://gitlab.com/cyrille_/projet-packer-ansible)
  -  [Pipeline 3 : Terraform](https://gitlab.com/cyrille_/projet-terraform)

## Déploiement d'une application, accès web par double authentification (MFA), reverse proxy

  -  Déploiement : Ansible, Packer, Terraform 
  -  Tests : Yammlint, Hadolint, Sonarqube
  -  Sécurité : Authelia, Traefik, Trivy
  -  Automatisation : GitlabCI
  -  Applicationis: Python, Flask

![](Images/gitlabci_authelia.png)

![](Images/projet_authelia.png)




